const historicoMsg = document.querySelector(".history") 
const button = document.querySelector(".buttonSend")
button.addEventListener('click',(event)=>{
    const message = document.createElement("div")
    const p = document.createElement("p")
    const editar = document.createElement("span")
    editar.innerText ="Editar"
    const excluir = document.createElement("span")
    editar.classList.add("edit")
    excluir.classList.add("delete")
    excluir.innerText ="Excluir"
    excluir.addEventListener("click",deletMessage)
    message.classList.add("message")
    const inputMessage = document.querySelector(".sendMessage")
    p.innerText = inputMessage.value;
    message.appendChild(p)
    message.appendChild(excluir)
    message.appendChild(editar)
    historicoMsg.appendChild(message)
    inputMessage.value = ""
    editar.addEventListener("click",event=>{
        const parent = event.target.parentNode
        const input = document.createElement("input")
        input.type = "text"
        message.appendChild(input)
        input.value =  p.innerText
        const save = document.createElement("button")
        save.innerText="Save"
        input.classList.add("input")
        save.classList.add("save")
        message.appendChild(save)
        save.addEventListener("click",event => {
            event.target.parentNode
            p.innerText = input.value
            input.value = ""
            input.classList.add("hide")
            save.classList.add("hide")
        })
    })
})

function deletMessage(e){
   const parent = e.target.parentNode
   parent.remove()
   console.log(parent)
}

